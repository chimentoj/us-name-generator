import pickle
import random
from operator import itemgetter

def init():
    first_names_path = "E:\\_Projects\\Names\\Code\\first_names.pkl"
    last_names_path = "E:\\_Projects\\Names\\Code\\last_names.pkl"

    with open(first_names_path, 'rb') as input_file:    
        first_names = pickle.load(input_file)

    with open(last_names_path, 'rb') as input_file:
        last_names = pickle.load(input_file)    

    year_start = 1980
    year_end = 2010

    number_of_names = 1
    
    names = []
    for name in range(0, number_of_names):
        year = '1982'
        #year = str(random.randint(year_start,year_end))
        WriteYearToFile(year,first_names[year])
        first_name = GetFirstName(first_names[year])
        last_name = GetLastName(last_names)
        names.append(first_name + " " + last_name)

    #for name in names:
        #print(name)       
    print(year)

def WriteYearToFile(year, names):
    names = [(n,s,int(y)) for n,s,y in names]
    with open(f"year_{year}.txt", 'w') as output_file:
        for name in sorted(names, reverse=True, key=itemgetter(2)):
            output_file.write(f"{name[0]}, {name[1]}, {str(name[2])}\n")

def GetFirstName(names_year):        
    print(names_year)
    return names_year[random.randint(0, len(names_year) - 1)][0]

def GetLastName(last_names):
    name = last_names[random.randint(0, len(last_names) - 1)]
    return name[0] + name[1:].lower()



if __name__ == '__main__':
    init()    