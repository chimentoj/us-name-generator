import os
import csv
import pickle

last_name_csv = "E:\\_Projects\\Names\\Data\\Top1000.csv"

last_names = []

with open(last_name_csv) as csv_file:
    line_count = 0
    for line in csv.reader(csv_file, delimiter=','):
        if line_count == 0:
            print(f'Column names are {", ".join(line)}')
            line_count += 1
        else:
            print(line[0])
            line_count += 1
            last_names.append(line[0])

with open('last_names.pkl', 'wb') as writer:
    pickle.dump(last_names, writer)