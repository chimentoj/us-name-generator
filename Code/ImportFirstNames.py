"""
Import the most popular First names in the United States based on census data
"""
import os
import pickle

def init():

    names = dict()

    first_name_folder = "E:\\_Projects\\Names\\Data\\FirstNames"
    first_name_files = [f for f in os.listdir(first_name_folder) if os.path.isfile(os.path.join(first_name_folder,f)) and os.path.splitext(os.path.join(first_name_folder,f))[1] == '.txt']

    for f in first_name_files:
        name_path = os.path.join(first_name_folder,f)
        names[GetYear(f)] = ParseNames(name_path)

    with open("first_names.pkl", "wb") as writer:
        pickle.dump(names, writer)

def ParseNames(file_path):    
    year_names = []
    with open(file_path) as reader:
        for line in reader.readlines():
            name_stats = line.split(',')
            year_names.append((name_stats[0], name_stats[1], name_stats[2]))
    return year_names

def GetYear(file_name):
    return file_name[3:7]


if __name__ == '__main__':
    init()